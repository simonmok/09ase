﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;
using System.IO;


namespace AssignmentDemo1 //namespace
{
    public partial class Form1 : Form //public class
    {/// <summary>
     /// This is the class
     /// </summary>
        static int curX = 0, curY = 0, param1 = 0, param2 = 0, param3 = 0, param4 = 0, param5 = 0, param6 = 0;
        static Pen myPen = new Pen(Color.Black, 2);
        static Pen myPos = new Pen(Color.Red, 1); //draw moveto
        static Pen erasePos = new Pen(Color.LightGray, 2); //erase movetos
        static CommandParser myParser = new CommandParser();
        static Brush myBrush = new SolidBrush(Color.Yellow);
        // static string array 

        static Point origin1 = new Point(0, 0); // reset position
        static Point origin2 = new Point(0, 0);
        public Form1()
        {
            InitializeComponent();
            Canvas.BackColor = Color.LightGray;
        } /// <summary>
          /// This is the form
          /// </summary>
          /// <param name="sender"></param>
          /// <param name="e"></param>

        private void Canvas_Paint(object sender, PaintEventArgs e)
        {

        }/// <summary>
         /// This is the CanvasPaint 
         /// </summary>
         /// <param name="sender"></param>
         /// <param name="e"></param>
        List<string> commands = new List<string>();
        void keyDownCommand(string command, bool bReadFormFile = false)
        {
            Graphics g = Canvas.CreateGraphics();
            Random randomNum = new Random();
            myPen.Color = Color.FromArgb(randomNum.Next(255), randomNum.Next(255), randomNum.Next(255));

            log.Text += command+"\r\n";


            string[] commandArray = command.Split(' '); //graphic x axis and y axis split 

            switch (commandArray[0]) //switch structure
            {
                case "drawto":
                    try
                    {
                        myParser.CheckParams(commandArray, ref param1, ref param2);
                        //通过Check,保存command到List
                        if (!bReadFormFile) commands.Add(command);
                        param1 = Int32.Parse(commandArray[1]);
                        param2 = Int32.Parse(commandArray[2]);
                        g.DrawLine(myPen, curX, curY, param1, param2);
                        curX = param1;
                        curY = param2;
                    }
                    catch (Exception ex)
                    {
                        log.Text += ex.Message + "\n";
                    }
                    break;
                case "moveto":
                    try
                    { //parameter error checking
                        myParser.CheckParams(commandArray, ref param1, ref param2);
                        //通过Check,保存command到List
                        if (!bReadFormFile) commands.Add(command);
                        g.DrawEllipse(erasePos, curX - 5, curY - 5, 10, 10);
                        g.DrawLine(myPen, param1, param2, param1, param2);
                        curX = param1;
                        curY = param2;
                        g.DrawEllipse(myPos, curX - 5, curY - 5, 10, 10);
                    }
                    catch (Exception ex)
                    {
                        log.Text += ex.Message + "\n";
                    }
                    break;
                case "rect":
                    try
                    {
                        #region 原代码
                        /*
                        myParser.CheckParams(commandArray, ref param1, ref param2);

                        g.DrawRectangle(myPen, curX, curY, param1, param2);
                        g.FillRectangle(myBrush, curX, curY, param1, param2);
                        curX = curX + param1;
                        curY = curY + param2;

                        */
                        #endregion
                        myParser.CheckParams(commandArray, ref param1, ref param2);
                        //通过Check,保存command到List
                        if (!bReadFormFile) commands.Add(command);

                        var sharp = new SharpFactory<TRect>(g);
                        sharp.SetFill(true);//实心矩形
                        sharp.SetBorder(myPen);
                        sharp.SetBackGroundColor(Color.Red);
                        sharp.MoveTo(curX, curY);
                        sharp.Draw(param1, param2);
                    }
                    catch (Exception ex)
                    {
                        log.Text += ex.Message + "\n";

                    }
                    break;
                case "triangle":
                    try
                    {
                        #region 原代码
                        /*
                    myParser.CheckParamstriangle(commandArray, ref param1, ref param2, ref param3, ref param4, ref param5, ref param6);
                    g.DrawLine(myPen, param1, param2, param3, param4);
                    g.DrawLine(myPen, param3, param4, param5, param6);
                    g.DrawLine(myPen, param5, param6, param1, param2);
                       */
                        #endregion

                        myParser.CheckParamstriangle(commandArray, ref param1, ref param2, ref param3, ref param4, ref param5, ref param6);
                        //通过Check,保存command到List
                        if (!bReadFormFile) commands.Add(command);

                        var sharp = new SharpFactory<TTriangle>(g);
                        sharp.SetFill(true);//实心三角形
                        sharp.SetBorder(myPen);
                        sharp.SetBackGroundColor(Color.Red);
                        sharp.MoveTo(param1, param2);
                        sharp.MoveTo(param3, param4);
                        sharp.MoveTo(param5, param6);
                        sharp.Draw(param1, param2);
                    }
                    catch (Exception ex)
                    {
                        log.Text += ex.Message+"\r\n";
                    }
                    break;
                case "circle":
                    try
                    {
                        myParser.CheckParams(commandArray, ref param1, ref param2);
                        //通过Check,保存command到List
                        if (!bReadFormFile) commands.Add(command);
                        g.DrawEllipse(myPen, param1, param2, param1 * 2, param2 * 2);
                        g.FillEllipse(myBrush, param1, param2, param1 * 2, param2 * 2);
                    }
                    catch (Exception ex)
                    {
                        log.Text = ex.Message;
                    }
                    break;
                case "reset":
                    Canvas.Refresh();
                    g.DrawLine(myPen, origin1, origin2);
                    curX = 0;
                    curY = 0;

                    //通过Check,清空commands
                    if (!bReadFormFile) commands.Clear();
                    break;
                case "exit":
                    if (System.Windows.Forms.Application.MessageLoop)
                    {
                        //WinForms app
                        System.Windows.Forms.Application.Exit();
                    }
                    break;
                case "load":
                    {
                        TFile file = new TFile();
                        file.LoadFromFileDialog();
                        if(file.GetLastErrorCode()!=-1)
                        {
                            log.Text += file.GetLastError() + "\n";
                        }
                        else
                        {
                            commands = file.GetCommands();
                            foreach (var item in commands)
                            {
                                keyDownCommand(item,true);
                            }
                        }
                        break;
                    }
                case "save":
                    {
                        TFile file = new TFile();
                       if(file.SaveFromSaveDialog(commands))
                        {

                        }
                       else
                        {
                            log.Text += file.GetLastError() + "\n";

                        }
                        break;
                    }
                default:
                    MessageBox.Show("error input");
                    break;
                    {

                    }
            }// switch
        }
        private void CommandLine_KeyDown(object sender, KeyEventArgs e)
        {
           

            if (e.KeyCode == Keys.Enter)
            {
                string command = CommandLine.Text;
                command = command.ToLower();

                //CommandLine.Clear(); //clear textbox
                string[] commandArray = command.Split(' '); //graphic x axis and y axis split 
             //   MessageBox.Show(command);

                keyDownCommand(command);


            }
        }
    }
}/// <summary>
 /// End with the gen graph
 /// </summary>

class program // ConsoleFileIO 
{/// <summary>
/// Console File IO
/// </summary>
/// <param name="args"></param>
    static void main(String[] args)
    {
        string filename = @"C:\Users\asd74\Desktop\ConsoleFile\DemoProg01.txt"; //filelocation

        try
        {
            List<string> prog = File.ReadAllLines(filename).ToList();
            foreach (String line in prog)
            {
                Console.WriteLine(line);
            }
            prog.Add("drawto 100 ");
            File.WriteAllLines(filename, prog);
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
        {

        }
    }
} 


