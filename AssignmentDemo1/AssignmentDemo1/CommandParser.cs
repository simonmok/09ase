﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentDemo1
{/// <summary>
/// 
/// </summary>
    class CommandParser //syntax checking and error checking 
    {/// <summary>
    /// This method initialuze Parser with normal graph 
    /// </summary>
    /// <param name="command"></param>
    /// <param name="arg1"></param>
    /// <param name="arg2"></param>
        public void CheckParams(String[] command, ref int arg1, ref int arg2) //check value in arg1 and arg2 , ref for return values after try catch
        {
            if (command.Length != 3) //try parser throw exception
            {
                throw new Exception("Command parameters error");
            }
            else if (!int.TryParse(command[1], out arg1))
            {
                throw new Exception("Command parameter 1 error");
            }
            else if (!int.TryParse(command[2], out arg2))
            {
                throw new Exception("Command parameter 2 error");
            }
            else if (arg1 <= 0 || arg2 <= 0) //valid values checking
            {
                throw new Exception("Please input value more than 0"); 
            }
            else if (arg1 > 530 || arg2 > 210) //scope checking
            {
                throw new Exception("Please input vaild values less than x=530 and y=210");
            }
        } /// <summary>
          /// This method initialuze Parser with triangle paramter 
          /// </summary>
          /// <param name="command"></param>
          /// <param name="arg1"></param>
          /// <param name="arg2"></param>
          /// <param name="arg3"></param>
          /// <param name="arg4"></param>
          /// <param name="arg5"></param>
          /// <param name="arg6"></param>
        public void CheckParamstriangle(String[] command, ref int arg1, ref int arg2, ref int arg3, ref int arg4, ref int arg5, ref int arg6) 
        {
            if (command.Length != 7) //try parser throw exception for maximum input
            {
                throw new Exception("total number Command parameters error");
            }
            else if (!int.TryParse(command[1], out arg1))
            {
                throw new Exception("Command parameter 1 error");
            }
            else if (!int.TryParse(command[2], out arg2))
            {
                throw new Exception("Command parameter 2 error");
            }
            else if (!int.TryParse(command[3], out arg3))
            {
                throw new Exception("Command parameter 3 error");
            }
            else if (!int.TryParse(command[4], out arg4))
            {
                throw new Exception("Command parameter 4 error");
            }
            else if (!int.TryParse(command[5], out arg5))
            {
                throw new Exception("Command parameter 5 error");
            }
            else if (!int.TryParse(command[6], out arg6))
            {
                throw new Exception("Command parameter 6 error");
            }
            else if (arg1 <= 0 || arg2 <= 0 || arg3 <= 0 || arg4 <= 0 || arg5 <= 0 || arg6 <= 0) //negative numbers checking
            {
                throw new Exception("Please input value more than 0");
            }
            else if (arg1 > 530 || arg2 > 220 || arg3 > 530 || arg4 > 220 || arg5 > 530 || arg6 > 220) // scope checking
            {
                throw new Exception("Please input vaild values with each point X sector less than 530 and Y sector less than 220");
            }
        } //method
    } //class 
}
